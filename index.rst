.. hannah documentation master file
   
.. toctree::
   :hidden: 
   
   userdoc/index
   development/index
   help
   license
   
.. image:: hannah_left_256.png
    :width: 256px
    :align: right
    :height: 256px
    :alt: hannah the watchcat
  
******
Hannah
******   
   
Hannah is going to be a multi user facility automation system.

What we are trying to achieve:

* Automate things - Everything with a switch/dial
* Automate User interaction - Generate User interfaces from configuration
* Automate Communication - Emit/Recieve communication on all channels
* Be secure - ssl everything, only expose whats nessesary
* Be safe - Authenticate/Authorize everything/everyone

How are we trying to achieve this:

* Be modular - One Tool for One job
* Loosely couple - Modules communicate over a messaging infrastructure, avoiding direct dependencies
* Use standards - Don't reinvent 
* Provide secure configuration defaults - Actually DEPEND on secure settings if insecure would also be possible
* Make every API/Interface Auth*-aware

Currently the initial Architecture is being drafted and implemented. Expect some useful stuff around mid 2015.

The Project is hosted on https://chaos.expert/hannah this documentation is generated from https://chaos.expert/hannah/docs

File your wishes/demands/irrelevant opinion here_ .

.. _here: https://chaos.expert/hannah/docs/issues

GPL V3 for code and Creative Commons Attribution 4.0 for everything else
