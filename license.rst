License
=======


Source Code
^^^^^^^^^^^

GPL V3

Objects - Text, Images
^^^^^^^^^^^^^^^^^^^^^^

CC-BY-SA 4.0
