User Documentation
==================

.. toctree::
   :maxdepth: 2
   :hidden: 
   
   installation
   configuration
   usage